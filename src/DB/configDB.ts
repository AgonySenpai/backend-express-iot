import { Sequelize } from 'sequelize';

const sequelize: Sequelize = new Sequelize({
	database: 'mqtt',
	host: 'localhost',
	dialect: 'mysql',
	password: 'prueba123',
	username: 'admin_bd',
	// username: 'root',
	port: 3306,
});

export default sequelize;
