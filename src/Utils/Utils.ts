import mqtt from 'mqtt';
import { Server } from 'socket.io';
import { Extra, Markup } from 'telegraf';
import { CallbackButton } from 'telegraf/typings/markup';
import { mqttClientMap, telegramBots } from '../Config/configMqtt';
import DeviceDataModel from '../Models/DeviceDataModel';
import DevicesModel, { IDeviceBase } from '../Models/DevicesModel';
import RulesModel from '../Models/RulesModel';
import UserModel from '../Models/UserModel';
import { formatDate } from './formatDate';

type IInitConnection = {
	username: string;
	password: string;
	ws: Server;
	topic: string;
	idUser: number;
};

export const initConnectionMQTT = ({
	username,
	password,
	topic,
	ws,
	idUser,
}: IInitConnection) => {
	const mqttClient = mqtt.connect('mqtt://peru-iot4.com', {
		port: 1883,
		host: 'peru-iot4.com',
		clientId:
			'access_control_server_' + Math.round(Math.random() * (0 - 10000) * -1),
		username: username,
		password: password,
		keepalive: 60,
		reconnectPeriod: 1000,
		protocol: 'mqtt',
		clean: true,
	});

	mqttClient.on('connect', (): void => {
		console.log('Conexión  MQTT Exitosa!');
		mqttClient?.subscribe(topic);
	});

	mqttClient.on('message', async (topic: string, payload: Buffer) => {
		ws.emit(
			topic,
			JSON.stringify({
				data: payload.toString(),
				topic,
			}),
		);
		await DeviceDataModel.build({
			data: parseInt(payload.toString()),
			topic,
		}).save();
	});
	mqttClientMap.get(idUser)?.end(true);
	mqttClientMap.delete(idUser);
	mqttClientMap.set(idUser, mqttClient);
};

export const initBotTelegram = async (userId: number, chatId: string) => {
	const bot = telegramBots.get(userId)!;

	bot.on('callback_query', async (ctx) => {
		const dataInfo = ctx.callbackQuery?.data;
		// Check if exists the data
		if (dataInfo) {
			const device = await DevicesModel.findByPk(parseInt(dataInfo));
			if (device) {
				const infoDevice = JSON.parse(device?.info) as IDeviceBase;
				const data = await DeviceDataModel.findOne({
					where: {
						topic: infoDevice.topic,
					},
					order: [['createdAt', 'DESC']],
				});
				//
				//
				if (data) {
					const date = data.createdAt;
					date.setHours(date.getUTCHours() - 5);
					await ctx.reply(
						`El ultimo valor del dispositivo ${infoDevice.panelName} es: ${
							data.data
						} con la fecha de ${formatDate(date)}`,
					);
				} else {
					await ctx.reply(`Topico ${infoDevice.topic} no encontrado`);
				}
			} else {
				await ctx.reply('Dispositivo no encontrado');
			}
		} else {
			await ctx.reply('Topico no valido');
		}
	});

	// Command for consult info device
	bot.command('dispositive', async (ctx) => {
		const devices = await DevicesModel.findAll({
			where: {
				UserId: userId,
			},
		});
		const options: Array<CallbackButton> = [];
		for (const device of devices) {
			const info = JSON.parse(device.info) as IDeviceBase;
			if (info.type !== 'graphic') {
				await options.push(
					Markup.callbackButton(info.panelName, `${device.id}`),
				);
			}
		}
		await ctx.reply(
			'Dispositivos',
			Extra.HTML().markup(Markup.inlineKeyboard(options)),
		);
	});

	// Consult the chatid
	bot.command('chatid', async (ctx) => {
		await ctx.reply(ctx.chat?.id.toString() || ':v');
	});

	// Consult the direction
	bot.command('direccion', async (ctx) => {
		const user = await UserModel.findByPk(userId);
		if (user) {
			await ctx.reply(`La direccion es: ${user.direction}`);
		} else {
			await ctx.reply('El usuario no existe');
		}
	});

	// Send Alert
	mqttClientMap.get(userId)?.addListener('message', async (topic, payload) => {
		const rules = await RulesModel.findAll({
			where: {
				UserId: userId,
			},
		});
		rules.forEach((rule) => {
			if (rule.state) {
				if (rule.topic === topic) {
					const value = parseInt(payload.toString());
					if (rule.comparison === '>=') {
						if (value >= rule.number) {
							bot.telegram.sendMessage(chatId, rule.message);
						}
					} else if (rule.comparison === '<=') {
						if (value <= rule.number) {
							bot.telegram.sendMessage(chatId, rule.message);
						}
					}
				}
			}
		});
	});
};
