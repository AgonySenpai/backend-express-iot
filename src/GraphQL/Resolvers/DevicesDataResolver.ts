import { Arg, Int, Mutation, Resolver } from 'type-graphql';
import DeviceDataModel from '../../Models/DeviceDataModel';
import { DevicesDataType } from '../typeDefs/DevicesDataType';
import { Op } from 'sequelize';
import moment from 'moment';
import 'moment/locale/es';
moment.locale('es');

@Resolver()
class DevicesDataResolver {
	@Mutation(() => [DevicesDataType]!)
	async getData(
		@Arg('topic') topic: string,
		@Arg('year', () => Int!) year: number,
		@Arg('month', () => Int!) month: number,
		@Arg('day', () => Int!) day: number,
		@Arg('hour', () => Int!) hour: number,
	) {
		const lastDate = new Date();
		lastDate.setUTCFullYear(year);
		lastDate.setUTCMonth(month);
		lastDate.setUTCDate(day);
		lastDate.setUTCHours(hour);
		lastDate.setUTCMinutes(0);
		lastDate.setUTCSeconds(0);
		lastDate.setUTCMilliseconds(0);
		const newDate = new Date();
		newDate.setUTCFullYear(year);
		newDate.setUTCMonth(month);
		newDate.setUTCDate(day);
		newDate.setUTCHours(hour + 1);
		newDate.setUTCMinutes(0);
		newDate.setUTCSeconds(0);
		newDate.setUTCMilliseconds(0);

		return await DeviceDataModel.findAll({
			where: {
				topic,
				createdAt: {
					[Op.between]: [lastDate, newDate],
				},
			},
		});
	}
}

export default DevicesDataResolver;
